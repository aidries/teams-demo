// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const axios = require('axios');
//require('axios-debug')(axios);
const _ = require('lodash');

const { ActivityHandler, CardFactory, MessageFactory, TeamsActivityHandler } = require('botbuilder');

class EchoBot extends TeamsActivityHandler {
    constructor() {
        super();
        // See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.
        this.onMessage(async (context, next) => {
            console.log('debug',context.activity);
            let payload = context.activity.text.replace(/<at>.*<\/at>/gm, '');
            payload = payload.replace(/\n$/gm, '');
            payload = payload.replace(/^\s/gm, '');

            const headers =
            {
                "Accept": "application/json",
                "Content-Type": "application/json; charset=UTF-8",
                "Origin": "https://www.mocd.gov.ae",
                "Referer": "https://www.mocd.gov.ae/",
                "Sec-Fetch-Mode": "cors",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
            }
            const res = await axios.post('https://gate.arabot.io/webhook/api/308/MOCD2018/send', {
                "botID": "308",
                "sourceType": "SMS",
                "customerKey": "MOCD2018",
                "keySecret": "fYSQacJfkPJeC_UU9LCL59C88hVYwiMHzNqtMHipBEfQLmwkJn0UJO3CG8j45eBX",
                "userObject": {
                    "userID": "arabot_a984b081-0b9f-4661-b7df-2c3eaefcba96",
                    "telephone": "0799999999",
                    "firstName": "محمد",
                    "lastName": "صلاح"
                },
                "message": payload
            }, {
                headers: headers
            });

            let responseText = "";
            const buttons = [];
            console.log(JSON.stringify(res.data.message));
            res.data.message.forEach(async element => {
                switch (element.send_type) {
                    case "TEXT":
                    case "NO_ANSWER":
                        console.log('44');
                        responseText += element.message.text;
                        break;
                    case "GENERIC":
                    case "BUTTON":
                        console.log('ayman', JSON.stringify(element.message.elements));
                        element.message.elements.forEach((item) => {
                            item.buttons.forEach((b) => {
                                if (b.url) {
                                    buttons.push(
                                        {
                                            type: 'openUrl',
                                            title: b.title,
                                            value: b.url
                                        })
                                } else {
                                    buttons.push(
                                        {
                                            type: 'imBack',
                                            title: b.title,
                                            value: b.payload
                                        })
                                }

                            })
                        })
                        await context.sendActivity({
                            attachments: [this.createHeroCard(responseText, buttons)]
                        });
                        break;
                    case "QUICK_REPLY":
                    case "NO_ANSWER_QUICK_REPLY":
                        console.log('ayman', JSON.stringify(element.message.elements));
                        element.message.elements.forEach((item) => {
                            if (item.url) {
                                buttons.push(
                                    {
                                        type: 'openUrl',
                                        title: item.title,
                                        value: item.url
                                    })
                            } else {
                                buttons.push(
                                    {
                                        type: 'imBack',
                                        title: item.title,
                                        value: item.payload
                                    })
                            }
                        })
                        await context.sendActivity({
                            attachments: [this.createHeroCard(responseText, buttons)]
                        });
                        break;
                    case "WEB_VIEW":
                        element.message.elements.forEach((item) => {
                            item.buttons.forEach((b) => {
                                responseText += b.title + '<br>' + b.subTitle
                            })
                        })
                        break;
                    case "VIDEO":
                        /*console.log('fsdfsdfsdfsdfs');
                        await context.sendActivity({
                            attachments: [this.createVideoCard(responseText, element.message.mediaUrl, null, null)]
                        });*/
                        const card = this.getHeroCardMenu();
                        const message = MessageFactory.attachment(card);
                        await context.sendActivity(message);
                        break;
                }
            });
            console.log('65');
            console.log(responseText)
            if (buttons.length == 0) {
                await context.sendActivity(responseText);
            }
            await next();


            // By calling next() you ensure that the next BotHandler is run.

        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; ++cnt) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    await context.sendActivity('Hello and welcome!');
                }
            }
            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });
    }

    createThumbnailCard(text, buttons, subtitle) {
        return CardFactory.thumbnailCard(
            text,
            [{ url: '' }],
            buttons,
            subtitle
        );
    }

    createHeroCard(title, buttons) {
        return CardFactory.heroCard(
            title,
            null,
            CardFactory.actions(buttons)
        );
    }

    createVideoCard(text, url, buttons, subtitle) {
        return CardFactory.videoCard(
            text,
            [{ url: url }]
        );
    }
    getHeroCardMenu() {
        return CardFactory.heroCard('Task Module Invocation from Hero Card',
            'This is a hero card with a Task Module Action button.  Click the button to show an Adaptive Card within a Task Module.',
            null, // No images
            [{ type: 'invoke', title: 'Task Module', value: { type: 'task/fetch', data: 'adaptivecard' } }]);
    }
    handleTeamsTaskModuleFetch(context, taskModuleRequest) {
        // taskModuleRequest.data can be checked to determine different paths.

        return {
            task: {
                type: 'continue',
                value: {
                    card: this.getTaskModuleAdaptiveCard(),
                    height: 220,
                    width: 400,
                    title: 'Adaptive Card: Inputs'
                }
            }
        };
    }

    async handleTeamsTaskModuleSubmit(context, taskModuleRequest) {
        return {
            task: {
                type: 'message', // This could also be of type 'continue' with a new Task Module and card.
                value: 'Hello. You said: ' + taskModuleRequest.data.usertext
            }
        };
    }

    getTaskModuleAdaptiveCard() {
        return CardFactory.adaptiveCard(
            {
                "type": "AdaptiveCard",
                "body": [
                    {
                        "type": "TextBlock",
                        "text": "Here is a ninja cat:"
                    },
                    {
                        "type": "Media",
                        "poster": "https://adaptivecards.io/content/poster-video.png",
                        "sources": [
                            {
                                "mimeType": "video/mp4",
                                "url": "https://adaptivecardsblob.blob.core.windows.net/assets/AdaptiveCardsOverviewVideo.mp4"
                            }
                        ]
                    }
                ],
                "version": "1.0"
            }
        );
    }
}

module.exports.EchoBot = EchoBot;
